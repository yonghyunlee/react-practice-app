import React, { Component } from 'react';
import './App.css';
import PhoneForm from "./components/PhoneForm";
import PhoneInfoList from "./components/PhoneInfoList";

class App extends Component {

  id = 2;

  state = {
    information: [
      {
        id: 0,
        name: '홍길동',
        phone: '010-1234-5678'
      },
      {
        id: 1,
        name: '김땡땡',
        phone: '010-3361-2519'
      }
    ]
  };

  handleCreate = (data) => {
    const { information } = this.state;

    this.setState({
      information: information.concat({
        id: this.id++,
        ...data
      })
    })
  };

  render() {
    const { information } = this.state;
    return (
      <div>
        <PhoneForm
          onCreate={this.handleCreate}/>
        <PhoneInfoList data={information}/>
      </div>
    );
  }
}

export default App;
